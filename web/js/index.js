$(document).on('pagebeforeshow', '#index', function(){ 
    ajax.ajaxCall("http://localhost:8083/", true);
});
 
$(document).on('click', '#user-list li', function(){ 
    ajax.ajaxCall("http://localhost:8083/" + $(this).data('listid'), false);
});
 
var ajax = { 
    parseJSON:function(result){ 
        var obj = jQuery.parseJSON(result);
        $('#user-list').empty();        
        $.each(obj, function(key,row) {
            $('#user-list').append('<li data-listid="' + key + '"><a href="" data-id="' + row.id + '"><h3>' + row.name + '</h3><p>' + row.profession + '</p></a></li>');       
        });
        $('#user-list').listview('refresh');        
    },
    parseJSONDetails:function(result){ 
        var obj = jQuery.parseJSON(result);     
        $('#personal-data').empty();
        $('#personal-data').append('<li>Name: '+obj.name+'</li>');
        $('#personal-data').append('<li>Profession: '+obj.profession+'</li>');           
        $('#personal-data').listview().listview('refresh'); 
        $.mobile.changePage( "#second");
    },
    ajaxCall: function(url, initialize) {
        $.ajax({
            url: url,
            async: 'true',
            success: function(result) {
                if (initialize) {
                    ajax.parseJSON(result);
                } else {
                    ajax.parseJSONDetails(result);
                }
            },
            error: function(request, error) {
                alert('Network error has occurred, please try again!');
            }
        });
    }
} 
